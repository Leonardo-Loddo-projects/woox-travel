// <!-- articolo 1 -->
// <div class="col-lg-8">

//   <div class="items">
//     <div class="row">
//       <div class="col-lg-12">
//         <div class="item">
//           <div class="row">
//             <div class="col-lg-4 col-sm-5">
//               <div class="image">
//                 <img src="assets/images/country-01.jpg" alt="">
//               </div>
//             </div>
//             <div class="col-lg-8 col-sm-7">
//               <div class="right-content">
//                 <h4>SVIZZERA</h4>
//                 <span>Europa</span>
//                 <div class="main-button">
//                   <a href="about.html">Esplora ancora</a>
//                 </div>
//                 <p>Woox Travel is a professional Bootstrap 5 theme HTML CSS layout for your website. You can use this layout for your commercial work.</p>
//                 <ul class="info">
//                   <li><i class="fa fa-user"></i> 8.66 Mil People</li>
//                   <li><i class="fa fa-globe"></i> 41.290 km2</li>
//                   <li><i class="fa fa-home"></i> $1.100.200</li>
//                 </ul>
//                 <div class="text-button">
//                   <a href="about.html">Need Directions ? <i class="fa fa-arrow-right"></i></a>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>

//       </div>

// catturo input barra di ricerca
let inputWord = document.querySelector('#inputWord');

// Dato di partenza
let destinations = [  
    {
      "img": "assets/images/country-01.jpg",
      "title": "Svizzera",
      "continent": "Europa",
      "body": "La Svizzera è un paese montuoso dell'Europa Centrale dove si trovano numerosi laghi, paesini e i picchi delle Alpi. Le sue città ospitano centri storici medievali.",
      "page": "about.html",
      "population": "8.66 Mil",
      "land": "41.290",
      "price": "1.100.200"
    },
    {
        "img": "assets/images/country-02.jpg",
        "title": "CARAIBI",
        "continent": "Nord America",
        "body": "I Caraibi sono una vasta regione geografica delle Americhe che comprende tutti i paesi bagnati dal Mare Caraibico.",
        "page": "about.html",
        "population": "44.48 Mil",
        "land": "275.400",
        "price": "946.000"
    },
    {
        "img": "assets/images/country-03.jpg",
        "title": "FRANCIA",
        "continent": "Europa",
        "body": "Situata nell'Europa occidentale, la Francia comprende città medievali, paesi alpini e spiagge sul Mediterraneo.",
        "page": "about.html",
        "population": "67.41 Mil",
        "land": "551.500",
        "price": "425.600"
    },
    {
        "img": "assets/images/country-04.png",
        "title": "BARI",
        "continent": "Europa",
        "body": "Culla della civiltá, patria di AuLab e sede di menti illustri come Nicola Menonna, é famosa per i furti di identitá su zoom. ",
        "page": "about.html",
        "population": "67.41 Mil",
        "land": "551.500",
        "price": "425.600"
    }
  ]

//catturo la row
let container = document.querySelector('#contenitore');

function createCards(array){
    container.innerHTML = '';
    //creare le card
    array.forEach((element) => {
    // creare la card
    let card = document.createElement('div');
    card.classList.add('col-lg-12');
    // riempire la card appena creata
    card.innerHTML = 
        ` <div class="items">
             <div class="row animazione" id='rowAbout'>
                 <div class="col-lg-12">
                 <div class="item">
                     <div class="row">
                     <div class="col-lg-4 col-sm-5">
                         <div class="image">
                         <img src="${element.img}" alt="">
                         </div>
                     </div>
                     <div class="col-lg-8 col-sm-7">
                         <div class="right-content">
                         <h4>${element.title}</h4>
                         <span>${element.continent}</span>
                         <div class="main-button">
                             <a href="${element.page}">Esplora ancora</a>
                         </div>
                         <p>${element.body}</p>
                         <ul class="info">
                             <li><i class="fa fa-user"></i> ${element.population} People</li>
                             <li><i class="fa fa-globe"></i> ${element.land} km2</li>
                             <li><i class="fa fa-home"></i> $${element.price}</li>
                         </ul>
                         <div class="text-button">
                             <a href="${element.page}">Need Directions ? <i class="fa fa-arrow-right"></i></a>
                         </div>
                      </div>
                 </div>
             </div>
         </div>`;
    // inserire l'elemento appena creato nel contenitore
    container.appendChild(card);
    })
}

// Abbiamo dato l'id dove deve agire la classe fadeIn
let colums = document.querySelectorAll('#rowAbout > *');

// Gestire animazione con evento scroll
document.addEventListener('scroll', () => {
    let scrollTop = window.scrollY;

    if (scrollTop > 550) {
        colums.forEach(element => {
            element.classList.add('animazione');
        })
    } 
    console.log(scrollTop);
})

//filtro per parola
function filterByWord(){
    inputWord.addEventListener('input', () => {
        let filtered = destinations.filter(destinazione => destinazione.title.toLowerCase().includes(inputWord.value.toLowerCase()));
        createCards(filtered);
    })
}

createCards(destinations);
filterByWord();